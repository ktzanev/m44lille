#!/bin/sh

title="M44_2022-23_Cours"

# check if the parameter is in [1,9]
# =============
if [ ! -f ${title}.tex ]; then
  echo "The file ${title}.tex do not exists."
  exit 1
else
  echo "Compile ${title} ..."
  echo "==================================="
fi

# clear folder
# =============
for f in temp.*; do
    ## check if temp files exist
    if [ -f "$f" ]; then
      rm temp.*
    fi
    ## This is all we needed to know, so we can break after the first iteration
    break
done

# presentation version
# ====================
echo ================================================
echo Build : ${title}_slides.pdf
echo ================================================
sd -sp '\PassOptionsToPackage{print}{m44cours}' '' "$title.tex" > temp.tex
tectonic temp.tex
mv temp.pdf ${title}_slides.pdf


# print version
# =============
echo ================================================
echo Build : ${title}_print.pdf
echo ================================================
# sed '1i\
# \\PassOptionsToPackage{print}{m44cours}' ${title}.tex > temp.tex
echo '\PassOptionsToPackage{print}{m44cours}' | cat - ${title}.tex > temp.tex
tectonic temp.tex
mv temp.pdf ${title}_print.pdf

# clear folder
# =============
rm temp.*
