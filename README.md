# [m44lille](https://gitlab.com/ktzanev/m44lille) / [web page](https://ktzanev.gitlab.io/m44lille/)

Les documents (plan du cours, feuilles de td...) du module M44 - « Géométrie » de la L2 Mathématiques à l'Université de Lille.

## 2023

Vous pouvez obtenir [ce dépôt](https://gitlab.com/ktzanev/m44lille) de deux façons faciles :

- en téléchargeant le [zip](https://gitlab.com/ktzanev/m44lille/-/archive/master/m44lille-master.zip) qui contient la dernière version des fichiers,
- en clonant le dépôt entier, l'historique y compris, en utilisant la commande `git` suivante

  ```shell
  git clone https://gitlab.com/ktzanev/m44lille.git .
  ```

Dans [ce dépôt](https://gitlab.com/ktzanev/m44lille) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec [tectonic](https://tectonic-typesetting.github.io)) des documents suivants :

- Plan détaillé du cours **[[pdf pour impression](Cours/M44_2022-23_Cours_print.pdf)]** [[pdf pour présentation](Cours/M44_2022-23_Cours_slides.pdf)] [[tex](Cours/M44_2022-23_Cours.tex)] _(document qui évoluera au cours du semestre)_
- Les feuilles de td :
  - TD n°1 **[[pdf](TDs/M44_2022-23_TD01.pdf)]** [[tex](TDs/M44_2022-23_TD01.tex)]
  - TD n°2 **[[pdf](TDs/M44_2022-23_TD02.pdf)]** [[tex](TDs/M44_2022-23_TD02.tex)]
  - TD n°3 **[[pdf](TDs/M44_2022-23_TD03.pdf)]** [[tex](TDs/M44_2022-23_TD03.tex)] _(avec annexe angles/arcs)_
  - TD n°4 **[[pdf](TDs/M44_2022-23_TD04.pdf)]** [[tex](TDs/M44_2022-23_TD04.tex)]
  - TD n°5 **[[pdf](TDs/M44_2022-23_TD05.pdf)]** [[tex](TDs/M44_2022-23_TD05.tex)]
  - TD n°6 **[[pdf](TDs/M44_2022-23_TD06.pdf)]** [[tex](TDs/M44_2022-23_TD06.tex)]
  - TD n°7 **[[pdf](TDs/M44_2022-23_TD07.pdf)]** [[tex](TDs/M44_2022-23_TD07.tex)]
  - TD n°8 **[[pdf](TDs/M44_2022-23_TD08.pdf)]** [[tex](TDs/M44_2022-23_TD08.tex)]
- Les sujets d'examens :
  - DS1 **[[sujet](Exams/M44_2022-23_DS1_sujet.pdf)]** **[[solutions](Exams/M44_2022-23_DS1_solutions.pdf)]** [[tex](Exams/M44_2022-23_DS1.tex)]
  - DS2 **[[sujet](Exams/M44_2022-23_DS2_sujet.pdf)]** **[[solutions](Exams/M44_2022-23_DS2_solutions.pdf)]** [[tex](Exams/M44_2022-23_DS2.tex)]
  - Rattrapage **[[sujet](Exams/M44_2022-23_Rattrapage_sujet.pdf)]** **[[solutions](Exams/M44_2022-23_Rattrapage_solutions.pdf)]** [[tex](Exams/M44_2022-23_Rattrapage.tex)]

Pour compiler ces fichiers vous avez besoin des styles et des logos suivants :

- pour les notes du cours
  - [m44cours.sty](Cours/m44cours.sty)
- pour les feuilles de td
  - [lille.sty](TDs/lille.sty)
  - [m44tds.tex](TDs/m44tds.tex)
  - [lille.pdf](TDs/images/lille.pdf)
- pour les examens
  - [lille.sty](Exams/lille.sty)
  - [m44exam.tex](Exams/m44exam.tex)
  - [lille.pdf](Exams/images/lille.pdf)
  - [attention.pdf](Exams/images/attention.pdf)
  - [tsvp.pdf](Exams/images/tsvp.pdf)

## 2022

Vous pouvez obtenir [la version de 2022](https://gitlab.com/ktzanev/m44lille/-/tree/v2022) de deux façons faciles :

- en téléchargeant le [zip](https://gitlab.com/ktzanev/m44lille/-/archive/v2022/m44lille-master.zip) qui contient la dernière version des fichiers,
- en clonant le dépôt, sans l'historique complète, en utilisant la commande `git` suivante

  ```shell
  git clone --depth 1 --branch v2022 https://gitlab.com/ktzanev/m44lille.git .
  ```

Dans [ce dépôt](https://gitlab.com/ktzanev/m44lille/-/tree/v2022) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX) des documents suivants :

- Plan détaillé du cours **[[pdf pour impression](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Cours/M44_2021-22_Cours_print.pdf)]** [[pdf pour présentation](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Cours/M44_2021-22_Cours_slides.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Cours/M44_2021-22_Cours.tex)] _(document qui évoluera au cours du semestre)_
- Les feuilles de td :
  - TD n°1 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD01.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD01.tex)]
  - TD n°2 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD02.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD02.tex)]
  - TD n°3 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD03.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD03.tex)] _(avec annexe angles/arcs)_
  - TD n°4 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD04.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD04.tex)]
  - TD n°5 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD05.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD05.tex)]
  - TD n°6 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD06.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD06.tex)]
  - TD n°7 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD07.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD07.tex)]
  - TD n°8 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD08.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/M44_2021-22_TD08.tex)]
- Les sujets d'examens :
  - DS1 **[[sujet](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Exams/M44_2021-22_DS1_sujet.pdf)]** **[[solutions](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Exams/M44_2021-22_DS1_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Exams/M44_2021-22_DS1.tex)]
  - DS2 **[[sujet](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Exams/M44_2021-22_DS2_sujet.pdf)]** **[[solutions](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Exams/M44_2021-22_DS2_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Exams/M44_2021-22_DS2.tex)]
  - Rattrapage **[[sujet](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Exams/M44_2021-22_Rattrapage_sujet.pdf)]** **[[solutions](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Exams/M44_2021-22_Rattrapage_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Exams/M44_2021-22_Rattrapage.tex)]


Pour compiler ces fichiers vous avez besoin des styles :

- [m44cours.sty](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/Cours/m44cours.sty) pour les notes du cours ;
- [m44tds.sty](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/m44tds.sty) pour les feuilles de td ;

ainsi que du [logo du département](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2022/TDs/math-ulille_noir.pdf).


## 2021

Vous pouvez obtenir [la version de 2021](https://gitlab.com/ktzanev/m44lille/-/tree/v2021) de deux façons faciles :

- en téléchargeant le [zip](https://gitlab.com/ktzanev/m44lille/-/archive/v2021/m44lille-master.zip) qui contient la dernière version des fichiers,
- en clonant le dépôt, sans l'historique complète, en utilisant la commande `git` suivante

  ```shell
  git clone --depth 1 --branch v2021 https://gitlab.com/ktzanev/m44lille.git .
  ```

Dans [ce dépôt](https://gitlab.com/ktzanev/m44lille/-/tree/v2021) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX) des documents suivants :

- Plan détaillé du cours **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/Cours/M44_2020-21_Cours.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/Cours/M44_2020-21_Cours.tex)] _(document qui évoluera au cours du semestre)_
- Une fiche sur les angles **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/Cours/angles/angles.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/Cours/angles/angles.tex)]
- Les feuilles de td :
  - TD n°1 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD01.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD01.tex)]
  - TD n°2 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD02.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD02.tex)]
  - TD n°3 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD03.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD03.tex)] _(voir fiche [angles/arcs](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/Cours/angles/angles.pdf))_
  - TD n°4 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD04.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD04.tex)]
  - TD n°5 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD05.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD05.tex)]
  - TD n°6 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD06.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD06.tex)]
  - TD n°7 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD07.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD07.tex)]
  - TD n°8 **[[pdf](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD08.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/M44_2020-21_TD08.tex)]
- Les sujets d'examens :
  - DS1 **[[sujet](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/exams/M44_2020-21_DS1_sujet.pdf)]** **[[solutions](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/exams/M44_2020-21_DS1_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/exams/M44_2020-21_DS1.tex)]
  - DS2 **[[sujet](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/exams/M44_2020-21_DS2_sujet.pdf)]** **[[solutions](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/exams/M44_2020-21_DS2_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/exams/M44_2020-21_DS2.tex)]
  - Rattrapage **[[sujet](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/exams/M44_2020-21_Rattrapage_sujet.pdf)]** **[[solutions](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/exams/M44_2020-21_Rattrapage_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/exams/M44_2020-21_Rattrapage.tex)]


Pour compiler ces fichiers vous avez besoin des styles :

- [m44cours.sty](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/Cours/m44cours.sty) pour les notes du cours ;
- [m44tds.sty](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/m44tds.sty) pour les feuilles de td ;

ainsi que du [logo du département](https://glcdn.githack.com/ktzanev/m44lille/-/raw/v2021/TDs/ul-fst-math_noir.pdf).

---
[Licence MIT](LICENSE)
