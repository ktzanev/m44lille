\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\lilleset{
  solutions=true,
  titre=\sisolutions{Solutions de l'examen}\sisujet{Examen},
  date=5 mai 2023,
  duree=3h,
}
\input{m44exam.tex}

\begin{document}
% ==================================
\sisujet{
  \attention~
  Documents non autorisés.

  \vspace{7mm}
  % \tsvp
}


%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, 4-5
\begin{exo}[.49]

  \begin{enumerate}
    \item Soient $ABCD$ un carré de côté $1$ inscrit dans un cercle et $E$ un point de ce cercle. Calculer $EA^2+EB^2+EC^2+ED^2$.
    \item Généraliser ce résultat pour un rectangle quelconque.
  \end{enumerate}
\end{exo}

\begin{solution}
  \emph{Il s'agit de l'exercice 11 de la fiche de TD n°3, non fait en TD.}
  \begin{center}
    \includegraphics{DS2_carre_dans_cercle}
    \hspace{1cm}
    \includegraphics{DS2_rectangle_dans_cercle}
  \end{center}
  \begin{enumerate}
    \item Comme les diagonales $AC$ et $DB$ sont des diamètres du cercle circonscrit, les triangles \hl{$\tri AEC$ et $\tri DEB$ sont rectangles}. Ainsi, par Pythagore, $\hl{EA^2+EC^2 =} AC^2 = AB^2+BC^2 = 1^2+1^2\hl{=2}$ et \hl{$ED^2+EB^2 = 2$}. Donc au final
    \[
      \hl{EA^2+EB^2+EC^2+ED^2 = 4}.
    \]
    \item Exactement le même raisonnement fonctionne pour un rectangle de côtés $a$ et $b$ en remplaçant la diagonale de $\sqrt{2}$ par une diagonale de $\sqrt{a^2+b^2}$. Ainsi on trouve
    \[
      \hl{EA^2+EB^2+EC^2+ED^2 = 2(a^2+b^2)}.
    \]
  \end{enumerate}
\end{solution}

%-----------------------------------
\begin{exo}

  Soit $\tri ABC$ un triangle isocèle avec $AB=AC > BC$. On porte des points $D$ sur $(AB)$, avec $B$ entre $A$ et $D$, et $E$ sur $(BC)$, avec $C$ entre $B$ et $E$, et tels que $BD=CE=AB-BC$.

  Montrer que $\tri ADE$ est un triangle isocèle.
\end{exo}

\begin{solution}
  \emph{Il s'agit de l'exercice 10 de la fiche de TD n°4, fait en TD}
  \imageR[y offset=28]{DS2_triangle_isocel}{
    Nous allons démontrer que les triangles $\tri DBE$ et $\tri ECA$ sont égaux.

    Comme le triangle $\tri ABC$ est isocèle en $A$ nous avons l'égalité des angles $\ang CBA = \ang BCA$, et donc de leurs complémentaires aussi: \hl{$\ang DBE = \ang ECA$}.
    D'après l'énoncé \hl{$BD=CE$}, mais aussi $\hl{BE=}BC+CE=AB\hl{=AC}$.
    Ainsi les triangles $\tri DBE$ et $\tri ECA$ ont deux côtés et un angle entre ces côtés égaux, donc sont égaux. Par conséquent \hl{$DE=EA$} et donc le triangle \hl{$\tri ADE$ est isocèle en $E$}.
  }
\end{solution}

% -----------------------------------------------
\begin{exo}

  Soient $A$ et $B$ deux points donnés à distance $AB=7$. Soient $P$ et $Q$ deux points tels que :
  \imageR[width=5cm, sep=7pt]{DS2_deux_cercles_et_tangente}{
    \begin{itemize}
      \item $AP=1$ et $BQ=4$;
      \item $A$ et $B$ sont du même côté de la droite $(PQ)$;
      \item $(PQ)$ est une tangente commune aux cercles $\mathcal{C}(A,P)$ et $\mathcal{C}(B,Q)$ \emph{(voir figure)}.
    \end{itemize}
  }
  \begin{enumerate}
    \item Calculer la longueur $PQ$.
    \item Donner un programme de construction des points $P$ et $Q$.\\
    \emph{La construction doit être effectuée à partir des points donnés $A$ et $B$. Les constructions classiques sont admises, y compris de la division d'un segment en $n$ parties égales.}
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item
      Comme $(PQ)$ est tangente aux deux cercles, nous avons \hl{$(AP) \perp (PQ) \perp (BQ)$}. Soit $C$ la projection orthogonale de $A$ sur $[BQ]$. Ainsi $ACQP$ est un rectangle et donc $CQ=1$ et $PQ = AC = \sqrt{AB^2-BC^2}$ d'après Pythagore. Pour finir $\hl{PQ=}\sqrt{7^2-3^2}=\hl{\sqrt{40}}$.
  \end{enumerate}
  \begin{center}
    \includegraphics[width=7cm]{DS2_deux_cercles_et_tangente_analyse}
  \end{center}
  \begin{enumerate}[resume]
    \item \textbf{Analyse.} Notons $D$ l'intersection de $(PQ)$ et $(AB)$. Les triangles $\tri DAP$ et $\tri ABC$ sont semblables car ont leurs trois côtés parallèles. Donc $\hl{DA =} AB\times\frac{PA}{CB} = 7\times\frac{1}{3} = \hl{\frac{7}{3}}$. Ainsi, comme $\ang DPA = \frac{\pi}{2}$ (resp. $\ang DQB = \frac{\pi}{2}$), une fois le point $D$ construit, on pourra construire $P$ (resp. $Q$) en intersectant les cercles $\mathcal{C}(A,1)\cap \mathcal{C}[DA]$ (resp. $\mathcal{C}(B,4)\cap \mathcal{C}[DB]$)

    \textbf{Constructions classiques.} On aura besoin des constructions classiques suivantes (pour $X$ et $Y$ deux points donnés), qui sont admises :
    \begin{itemize}
      \item \emph{Division d'un segment $[XY]$ en $n$ parties égales.} Cette construction nous permet de construire $n-1$ points $Z_{1},\dots,Z_{n-1}$ du segment $[XY]$ tels que $XZ_{i} = \frac{i}{n}XY$ et $Z_{i}Y = \frac{n-i}{n}XY$.
      \item \emph{Le cercle de diamètre $[XY]$.} Soit $O$ le milieu du segment $[XY]$ obtenu en divisant le segment en $2$ parties égales, par exemple avec la construction précédente. Alors le cercle $\mathcal{C}(O,X)$ est le cercle recherché $\mathcal{C}[XY]$.
    \end{itemize}
    \textbf{Programme de construction.}
    \begin{enumerate}[1.]
        \item On divise le segment $[AB]$ en $7$ parties égales. On note $M_1,\dots,M_{6}$ les points obtenus, de sorte que $AM_{1}=\frac{1}{7}\times7=1$ et $M_{3}B=\frac{7-3}{7}\times7=4$. Ainsi \hl{$\mathcal{C}(A,M_1) = \mathcal{C}(A,1)$} et \hl{$\mathcal{C}(B,M_3) = \mathcal{C}(B,4)$}.
    \end{enumerate}
    \vskip -7pt
    \imageR[width=9cm, offset={-28}{0}, sep=-7mm]{DS2_deux_cercles_et_tangente_construction}{%
      \begin{enumerate}[1.,resume]
        \item On divise le segment $[AB]$ en $3$ parties égales. On note $N$ le point obtenu tel que $AN=\frac{1}{3}\times AB = \frac{7}{3}$.
        \item $D \coloneqq \mathcal{C}^*(A,N) \cap (AB)$ de sorte que \hl{$AD = \frac{7}{3}$}.
        \item $\{\hl{P},P'\} \coloneqq \hl{\mathcal{C}[DA]\cap \mathcal{C}(A,M_1)}$.
        \item $\{\hl{Q},Q'\} \coloneqq \hl{\mathcal{C}[DB]\cap \mathcal{C}(B,M_3)}$ avec $Q$ du même côté de la droite $AB$ que $P$.
      \end{enumerate}
    }\vskip 2mm
    \emph{Remarque : Les points $P'$ et $Q'$, symétriques à $P$ et $Q$ par rapport\\ à la droite $(AB)$, conviennent aussi.}
  \end{enumerate}
\end{solution}

% -----------------------------------------------
\begin{exo}

  Soient $O$ l'origine d'affixe $0$ et $A$, $B$ et $C$ trois points distincts d'un cercle de centre $O$, d'affixes respectives $a$, $b$ et $c$.
  \begin{enumerate}
    \item Démontrer que le point $H$ d'affixe $h = a+b+c$ est l'orthocentre du triangle $\tri ABC$.
    \item Déterminer l'affixe $g$ du barycentre $G$ du triangle $\tri ABC$.
    \item Montrer que les points $O$, $G$ et $H$ sont alignés.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Pour montrer que $H$ est l'orthocentre, il suffit de montrer que $(AH) \perp (BC)$, $(BH) \perp (CA)$ et $(CH) \perp (AB)$.
    Nous utiliserons $\hl{(AH) \perp (BC) \iff} \scalprod{\vv{AH}}{\vv{BC}} = 0 \iff \hl{\Re(h-a)\overline{(c-b)} = 0}$. Commençons par $(h-a)\overline{(c-b)}=(b+c)(\overline{c}-\overline{b}) = b\overline{c} + c\overline{c} - b\overline{b} - c\overline{b} = b\overline{c} - c\overline{b}$, car $c\overline{c} = b\overline{b} = r^{2}$, où $r$ est le rayon du cercle sur lequel sont situés les points $A$, $B$ et $C$. Ainsi, comme $\Re(b\overline{c}) = \Re(c\overline{b})$, vu que $\overline{b\overline{c}} = c\overline{b}$, on trouve que $\Re(h-a)\overline{(c-b)} = 0 \iff (AH) \perp (BC)$, autrement dit \hl{$(AH)$ est une hauteur}.

    La démonstration que $(BH)$ et $(CH)$ sont aussi des hauteurs\footnote{En réalité une seule des deux suffit.} est identique. Ainsi \hl{$H$ est l'orthocentre de $\tri ABC$}.
    \item Comme $G=\frac{1}{3}A+\frac{1}{3}B+\frac{1}{3}C$ nous avons \hl{$g=\frac{a+b+c}{3} = \frac{h}{3}$}.
    \item Comme les affixes de $\vv{OG}$ et $\vv{OH}$ sont respectivement $g = \frac{h}{3}$ et $h$, on trouve que les deux vecteurs sont colinéaires avec $\hl{3\vv{OG}=\vv{OH}}$.
    Ainsi les points \hl{$O$, $G$ et $H$ sont alignés}\footnote{La droite qui contient ces 3 points est dite \emph{la droite d'Euler}, vue en cours.}.
  \end{enumerate}
\end{solution}

\end{document}
