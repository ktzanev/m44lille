\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\lilleset{
  solutions=true,
  titre=\sisolutions{Solutions du devoir surveillé}\sisujet{Devoir surveillé},
  date=2 mars 2023,
  duree=2h,
}
\input{m44exam.tex}

\begin{document}
% ==================================
\sisujet{
  \attention~
  Documents non autorisés.

  \vspace{17mm}
  % \tsvp
}


%-----------------------------------
%Posamentier & Salkind - Challenging Problems in Geometry, page 2
\begin{exo}[.7]

  Soient un triangle $\tri ABC$ rectangle en $C$, $H$ le pied de la hauteur issue de $C$ et $M$ le milieu de $AB$. Montrer que la bissectrice de $\ang ACB$ est aussi bissectrice de $\ang MCH$.\\
  \emph{[Question bonus]} Est-ce encore vrai pour un triangle qui n'est pas rectangle en $C$ ?
\end{exo}

\begin{solution}

  \emph{Il s'agit de l'exercice 4 de la feuille de TD n°3, fait en TD.}

  \textbf{Méthode I.}
  \imageR[width=49mm, sep=7mm, y offset=17, trim={28 0 7 0}]{DS1_bisectrice_hauteur_mediane1}{
    Pour démontrer que la bissectrice $CL$ est aussi bissectrice de $\ang MCH$ \hl{il suffit de montrer que $\ang ACM = \ang HCB$}. En effet si c'est le cas $\hl{\ang MCL =} \ang ACL - \ang ACM = \ang BCL - \ang BCH \hl{= \ang HCL}$.
    Nous avons d'une part $\hl{\ang HCB =} \frac{\pi}{2} - \ang CBA = \hl{\ang BAC}$.
  }\vskip 1mm
  D'autre part, comme $M$ est le centre du cercle circonscrit du triangle rectangle $\tri ABC$, nous avons $MA=MC$ et donc le triangle $\tri AMC$ est isocèle en $M$. Ce qui nous donne l'égalité $\hl{\ang BAC = \ang ACM}$. Pour finir, on vient de trouver l'égalité souhaitée $\hl{\ang ACM} = \ang BAC = \hl{\ang HCB}$.

  \textbf{Méthode II.}

  Cette méthode nous permet de répondre aussi à la question bonus, car elle démontre que $CL$ est la bissectrice de $\ang MCH$ si et seulement si le triangle $\tri ABC$ est rectangle ou isocèle en $C$. Pour démontrer ce résultat nous allons démontrer que, dans tout triangle, $CL$ est aussi la bissectrice de $\ang OCH$, où $O$ est le centre du cercle circonscrit.

  \emph{Soit dans la suite $\tri ABC$  un triangle quelconque, pas forcément rectangle.}
  \imageR[width=49mm, sep=7mm]{DS1_bisectrice_hauteur_mediane2}{
    On note $P$ l'intersection de la médiatrice de $[AB]$ et du cercle $\mathcal{C}$ circonscrit au $\tri ABC$. Comme $PA=PB$, nous avons l'égalité des (petits) arcs $\wideparen{PA}=\wideparen{PB}$. Ce qui se traduit par l'égalité des angles qui éclairent ces arcs depuis le cercle, $\ang ACP = \ang PCB$. Autrement dit $CP$ est la bissectrice de l'angle $\ang ACB$\footnote{La bissectrice et la médiatrice se rencontre sur le cercle circonscrit.}.
    Montrons que $CP$ est aussi la bissectrice de $\ang OCH$. Comme le triangle $\tri COP$ est isocèle en $O$, nous avons $\hl{\ang OCP = \ang CPO}$. Et comme $(MP) \perp (AB) \perp (CH)$, nous avons l'égalité entre les angles alternes-internes $\hl{\ang CPO = \ang PCH}$.
  }

  En conclusion, comme \hl{$CL$ est} la bissectrice de $\ang OCH$, pour qu'elle soit \hl{aussi la bissectrice de $\ang MCH$} il faut et il suffit que $(CO) = (CM)$. Mais $(CO) = (CM)$ \hl{si et seulement si $O=M$} (le triangle est \hl{rectangle} car le côté $[AB]$ est un diamètre) \hl{ou si $C \in (MO)$} (le triangle est \hl{isocèle} car $C$ est sur la médiatrice de $[AB]$).

\end{solution}

%-----------------------------------
\begin{exo}[.49]

  Soit $A,B,C$ trois point non alignés, qui forment un repère affine $\mathcal{R}$. On considère le triangle $\tri ABC$ dont les milieux des côtés sont notés $A',B',C'$, et $M$ le point de coordonnées barycentriques $(\frac{1}{4},\frac{1}{4},\frac{1}{2})_{\mathcal{R}}$, c.-à-d. $M = \frac{1}{4} A + \frac{1}{4} B + \frac{1}{2} C$.
  \begin{enumerate}
    \item Montrer que $M \in [CC']$.
    \item Chercher les coordonnées barycentriques de $P,Q,R$, points symétriques de $M$ par rapport aux points $A',B',C'$ respectivement.
    \item Montrer que les milieux de $[AP]$, $[BQ]$ et $[CR]$ coïncident.
    \item Représenter, le plus fidèlement possible, la position relative de tous ces points sur un graphique.
  \end{enumerate}
\end{exo}

\begin{solution}

  \emph{Il s'agit d'un exercice très proche de l'exercice 5 de la feuille de TD n°2, non fait en TD.}

  \begin{enumerate}
    \item Comme $C'=\frac{1}{2}A+\frac{1}{2}B$ nous avons $M=\frac{1}{2}(\frac{1}{2}A+\frac{1}{2}B) + \frac{1}{2}C = \frac{1}{2}C'+\frac{1}{2}C$. Donc $M$ est le milieu de $[CC']$.
    \item Nous avons
    \begin{itemize}
      \item $A' = \frac{1}{2}P+\frac{1}{2}M$, donc $\hl{P =} 2A'- M = B + C - (\frac{1}{4} A + \frac{1}{4} B + \frac{1}{2} C) = \hl{-\frac{1}{4} A + \frac{3}{4} B + \frac{1}{2} C}$
      \item $B' = \frac{1}{2}Q+\frac{1}{2}M$, donc $\hl{Q =} 2B'- M = C + A - (\frac{1}{4} A + \frac{1}{4} B + \frac{1}{2} C) = \hl{\frac{3}{4} A - \frac{1}{4} B + \frac{1}{2} C}$
      \item $C' = \frac{1}{2}R+\frac{1}{2}M$, donc $\hl{R =} 2C'- M = A + B - (\frac{1}{4} A + \frac{1}{4} B + \frac{1}{2} C) = \hl{\frac{3}{4} A + \frac{3}{4} B - \frac{1}{2} C}$
    \end{itemize}
    \item Nous allons montrer que $N \coloneqq \frac{3}{4} A + \frac{3}{4} B + \frac{1}{2} C $ est le milieu des trois segments.
    \begin{itemize}
       \item $\hl{\frac{1}{2}(A + P) =} (A - \frac{1}{4} A + \frac{3}{4} B + \frac{1}{2} C) = \frac{3}{4} A + \frac{3}{4} B + \frac{1}{2} C \hl{= N}$ ;
       \item $\hl{\frac{1}{2}(B + Q) =} (B + \frac{1}{4} A - \frac{3}{4} B + \frac{1}{2} C) = \frac{3}{4} A + \frac{3}{4} B + \frac{1}{2} C \hl{= N}$ ;
       \item $\hl{\frac{1}{2}(C + R) =} (C + \frac{3}{4} A + \frac{3}{4} B - \frac{1}{2} C) = \frac{3}{4} A + \frac{3}{4} B + \frac{1}{2} C \hl{= N}$ ;
    \end{itemize}
    \item Les points $Q, B', M, A', P$ sont équirépartis sur la droite de milieu. Les points $C, M, C', R$ sont équirépartis sur la médiane issue de $C$. Le point $N$ est le milieu de $[MC']$.
    \begin{center}
      \includegraphics{DS1_barycentre_symetries}
    \end{center}
  \end{enumerate}
\end{solution}

%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, page 7
\begin{exo}

  Soient un triangle $\tri ABC$, et des points $D,F,H \in [AC]$ et $E,G \in [BC]$ tels que $AB \parallel DE \parallel FG$ et $BD \parallel EF \parallel GH$.
  \begin{enumerate}
    \item Déterminer la longueur $AD$ en sachant que $CF = 2$ et $FD=1$.
    \item Déterminer la longueur $AC$ en fonction de $CH=m$ et $HF=n$.
  \end{enumerate}
\end{exo}

\begin{solution}
  \newcommand*{\eqt}[2]{{\color{#2}\stackrel{\scalebox{.5}{$\tri #1$}}{=\joinrel=\joinrel=}}}

  \emph{Il s'agit de l'exercice 4 de la feuille de TD n°4, non fait en TD.}


  Toutes les égalités entre rapports sont justifiées par le théorème de Thalès. Quand il s'agit d'une égalité justifiée par le théorème de Thalès pour le triangle $\tri XYZ$ on va écrire \enquote{$\eqt{XYZ}{black}$}.

  \imageR[width=49mm, sep=14pt, y offset=-7, trim={42 0 0 0}]{DS1_triangle_droites}{
  \begin{enumerate}
      \item Nous avons $\hl{AD:CD} \eqt{ACB}{blue} BE:CE \eqt{DCB}{red} DF:FC \hl{= 1:2}$. Et comme $\hl{CD}=1+2\hl{=3}$, on trouve $\hl{AD = \frac{3}{2}}$.
      \item Comme \hl{$CF = m+n$}, nous avons
      \[
        \hl{(m+n):m =} CF:CH \eqt{FCE}{red} CE:CG \eqt{DCE}{blue} \hl{CD:CF}.
      \]
      Ainsi on trouve $\hl{CD =} \frac{m+n}{m}CF = \hl{\frac{(m+n)^2}{m}}$. De plus
      \[
        \hl{(m+n):m =} CD:CF \eqt{DCB}{red} CB:CE \eqt{ACB}{blue} \hl{CA:CD},
      \]
      et donc $\hl{CA =} \frac{m+n}{m}CD = \hl{\frac{(m+n)^3}{m^2}}$.
  \end{enumerate}
  }

\end{solution}


\end{document}
