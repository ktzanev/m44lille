\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m44tds.tex}

\lilleset{titre=TD1 : Le plan euclidien}

\begin{document}


% ==================================
\section{Métrique euclidienne}
% ==================================

\begin{convention}
  Le plan cartésien $\R^2$ est noté $\ens{P}$. Il est muni de la distance euclidienne
  \[
    AB=\sqrt{(x_B-x_A)^2+(y_B-y_A)^2}
  \]
  pour tous $A=(x_A,y_A)$, $B=(x_B,y_B)$ dans $\ens{P}$.

  Pour un point $M$ et un ensemble $\mathcal{X}$ on note la distance de $M$ à $\mathcal{X}$ par
  $$
    d(M,\mathcal{X}) \coloneqq \inf_{X \in \mathcal{X}} MX.
  $$
  Et on dit que $N \in \mathcal{X}$ réalise la distance $d(M,\mathcal{X})$ si $MN = d(M,\mathcal{X})$.
\end{convention}

%-----------------------------------
\begin{exo} (Projeté orthogonal)

  Soient $A$ un point et $\ens{D}$ une droite du plan.
  On rappelle que le \emph{projeté orthogonal} $\pi_{\ens{D}}(A)$ de $A$ sur $\ens{D}$ est l'intersection de l'unique droite perpendiculaire\footnote{Deux droites sont perpendiculaires si elles se coupent en formant un angle droit, ou encore si tout vecteur directeur de l'une est orthogonal à tout vecteur directeur de l'autre.} à $\ens{D}$ passant par $A$.
  \begin{enumerate}
     \item Exprimer les coordonnées du projeté $H$ en fonction de celles de $A$ et d'une équation de $\ens{D}$.
     \item Montrer que $H$ réalise la distance de $A$ à $\mathcal{D}$.
     \item Montrer que $H$ réalise la distance de $A$ au demi-plan délimité par $\mathcal{D}$ et ne contenant pas $A$. Quel est le point qui réalise la distance entre $A$ et l'autre demi-plan ?
     \item Étant donnés deux points $B,C \in \mathcal{D}$, quel est le point qui réalise la distance de $A$ au segment $[BC]$ ?
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Intersection droite/cercle)

  \begin{enumerate}
    \item Étudier l'intersection d'une droite $\ens{D}$ et d'un cercle $\ens{C}(A,r)$.
    \item Montrer que quand $\ens{D} \cap \ens{C}(A,r) = \left\{ H \right\}$ (resp. $\left\{ M,N \right\}$), alors le projeté orthogonal de $A$ sur $\ens{D}$ est $H$ (resp. le milieu de $[M,N]$).
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Inégalité triangulaire)

  Soient $A,B,C$ trois points du plan.
  \begin{enumerate}
    \item Donner une paramétrisation de la droite $(BC)$. Comment est paramétré le segment $[BC]$ ?
    \item Montrer que si $A \in (BC)$, alors $AB+BC \geq AC$ avec égalité si et seulement si $A \in [BC]$.
    \item Pour $A \neq C$, en considérant le projeté orthogonal de $B$ sur $(AC)$, montrer que
    \[
      AC \leqslant AB+BC,
    \]
    avec égalité si et seulement si $B \in [AC]$. Et si $A=C$ ?
    \item Retrouver cette inégalité (et le cas d'égalité) en rappelant que $AB^{2}=\vv{AB}\cdot\vv{AB}$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Intersections de cercles)
  \begin{enumerate}
    \item Étudier l'intersection des deux cercles $\ens{C}(A_1,r_1)$ et $\ens{C}(A_2,r_2)$ en fonction de la distance $d\coloneqq A_{1}A_{2}$ entre les centres et des deux rayons $r_{1}$ et $r_{2}$.
    \item Sous quelle condition sur les réels $a$, $b$, $c$ existe-t-il un triangle dont les côtés ont pour longueurs $a$, $b$ et $c$? Comment se simplifie cette condition si $a \leqslant b$? et si $a \leqslant b \leqslant c$?
  \end{enumerate}
\end{exo}


%-----------------------------------
\begin{exo} (Convexes et inéquations)

  \begin{enumerate}
    \item Donner un système d'inéquations linéaires dont l'ensemble de solutions est le carré unité.
    \item Soient $A(1,2)$,$B(4,1)$ et $C(3,4)$ trois points du plan euclidien. Donner un système d'inéquations linéaires dont la solution est l'intérieur du triangle $\tri ABC$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Projection sur un convexe)

  \begin{enumerate}
    \item Montrer que $O(0,0)$ réalise la distance de $M(-1,-1)$ au carré unité. Combien vaut cette distance ?
    \item Soient $A(1,2)$,$B(4,1)$ et $C(3,4)$ trois points du plan euclidien. Montrer que $A$ réalise la distance de $M(-1,-1)$ au triangle $\tri ABC$. Combien vaut cette distance ?
    \item Quel point réalise la distance de $N(1,0)$ au triangle $\tri ABC$. Combien vaut cette distance ?
  \end{enumerate}
\end{exo}


% ==================================
\section{Le théorème de Pythagore}
% ==================================


%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, 3-2
\begin{exo}[.49]

  Dans le triangle $\tri ABC$, la médiane $AD$ est perpendiculaire à la médiane $BE$. Trouver $AB$ en sachant que $BC = 6$ et $AC = 8$.
\end{exo}


%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, 3-3
\begin{exo}

  Sur l'hypoténuse $AB$ du triangle rectangle $\tri ABC$, tracer à l'extérieur du triangle le carré $ABLH$. En sachant que $AC = 6$ et $BC = 8$, trouver $CH$.
\end{exo}


%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, 3-4
\begin{exo} \emph{(DS2, 2022)}

  Les mesures des côtés d'un triangle rectangle sont $60$, $80$ et $100$. Soit un point sur l'hypoténuse, qui divise le triangle en deux triangles de périmètre égal. Trouvez la distance entre ce point et le sommet de l'angle droit.
\end{exo}

%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, 3-5
\begin{exo} \emph{(Rattrapage, 2022)}

  Sur les côtés $AB$ et $DC$ du rectangle $ABCD$, les points $F$ et $E$ sont choisis de manière à ce que $AFCE$ soit un losange. Si $AB = 16$ et $BC = 12$, trouvez $EF$.
\end{exo}

%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, 3-12
\begin{exo}

  Soient un triangle $\tri ABC$, $P$ un point à l'intérieur du triangle dont les projections sur les côtés du triangle sont respectivement $A' \in [BC]$, $B' \in [CA]$ et $C' \in [AB]$. En sachant que $AC'=12$, $C'B=6$, $BA'=8$, $A'C=14$ et $CB'=13$, déterminer la longueur de $B'A$.
\end{exo}

%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, 4-28
\begin{exo}[.49] \emph{(DS1, 2021)\footnote{En version \enquote{Construction à la règle et compas} comme en TD5.}}

  Un cercle de rayon $3$ est inscrit dans un carré. Un deuxième cercle est inscrit entre le carré et le premier cercle. Déterminer le rayon du deuxième cercle.
\end{exo}

\end{document}
