\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m44tds.tex}

\lilleset{titre=TD6 : Courbes paramétrées}

\begin{document}


% ==================================
\section{Droite}
% ==================================

%-----------------------------------
\begin{exo} (Paramétrisation barycentrique)

  Soient $A$, $B$ et $C$ trois points non alignés.
  \begin{enumerate}
    \item Montrer que $\lambda \mapsto (1-\lambda)A + \lambda B$ est une paramétrisation régulière de la droite $(AB)$.
    \item Sous quelle condition cette paramétrisation est « par longueur d'arc » ?
    \item Donner une paramétrisation par longueur d'arc de $(AB)$.
    \item Donner une paramétrisation du segment $[AB]$ et une de la demi-droite $[BA)$.
    \item Étudier la courbe $G:\theta\mapsto G_{\theta}$, où $G_{\theta}$ est le barycentre de $A$ et $B$ avec les poids respectifs $\big(\cos^{2}(\theta),\sin^{2}(\theta)\big)$.
    \item Soit $I \coloneqq [\theta_A,\theta_B]$ un intervalle sur lequel $G$ est injective et telle que $G_{\theta_A} = A$ et $G_{\theta_B} = B$. Montrer que la longueur de $G|_{I}$ ne dépend pas du choix de $I$ et calculer la.
  \end{enumerate}
\end{exo}


% ==================================
\section{Cercle}
% ==================================

%-----------------------------------
\begin{exo} (Paramétrisations standards)

  On considère la courbe paramétrée $\mathcal{C} : \theta \mapsto \vvec{a + r\cos(\theta)}{b + r\sin(\theta)}$.
  \begin{enumerate}
    \item Montrer qu'il s'agit d'une paramétrisation régulière du cercle $C(O,r)$ avec centre $O(a,b)$.
    \item Sous quelle condition cette paramétrisation est « par longueur d'arc » ?
    \item Donner une paramétrisation par longueur d'arc de $C(O,r)$.
    \item Montrer que la tangente au point $\mathcal{C}(\theta)$ est perpendiculaire à la droite $\big(O\mathcal{C}(\theta)\big)$.
    \item Donner une paramétrisation de la tangente au point $\mathcal{C}(\theta)$.
    \item Donner une paramétrisation de la tangente au point $P(c,d) \in C(O,r)$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}\label{cerclerat} (Paramétrisation rationnelle du cercle)

  Soit $\ens{C}$ le cercle unité dans le plan, c.-à-d. le cercle de centre $O=(0,0)$ et de rayon $1$. Soient $P$ le point $(-1,0)$ et $\ens{D}$ la droite d'équation $x=0$.
  \begin{enumerate}
    \item Montrer que pour tout point $M \in \ens{D}$ la droite $(PM)$ recoupe $\ens{C}$ en exactement un point $Q \neq P$.
    \item Montrer que l'application $M \mapsto Q$ est une bijection de $\ens{D}$ sur $\ens{C}^\ast=\ens{C} \setminus \{P\}$.
    \item En déduire que $\rho \colon t \longmapsto \left( \dfrac{1-t^2}{1+t^2}, \dfrac{2t}{1+t^2}\right)$ établit une bijection de $\R$ sur $\ens{C}^\ast$. En donner la réciproque.
  \end{enumerate}
  \begin{convention}
      On dit que $\rho$ est une \emph{paramétrisation rationnelle} du cercle $\ens{C}$: c'est une application qui atteint presque tous les points du cercle et dont les coordonnées sont des fractions rationnelles en la variable $t$.
  \end{convention}
  \begin{enumerate}[resume]
    \item Montrer que $\rho(t) \in \Q^2$ si et seulement si $t \in \Q$.
    \item Soit $Q$ un point de $\ens{C}$ distinct de $P$. Quelles sont les coordonnées du point d'intersection de la droite $(PQ)$ et de la droite d'équation $x=1$?
    \item En utilisant la paramétrisation habituelle du cercle $\theta \in \R \mapsto (\cos \theta,\sin \theta)$, retrouver l'expression de $\rho$ (on pourra interpréter $t$ comme une certaine ligne trigonométrique reconnaître une formule connue).
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Triplets pythagoriciens)

  On cherche tous les triangles rectangles dont les côtés sont de longueur entière.
  \begin{enumerate}
    \item Montrer que cela revient à résoudre dans $\Z^3$ l'équation $a^2+b^2=c^2$.
    \item Soit $(a,b,c)$ une solution non nulle. Montrer, en utilisant l'exercice \ref{cerclerat}, qu'il existe $t \in \Q$ tel que $\dfrac{a}{c}= \dfrac{1-t^2}{1+t^2}$ et $\dfrac{b}{c}= \dfrac{2t}{1+t^2}$.
    \item En déduire qu'un triangle rectangle a tous ses côtés de longueur entière si et seulement si ses côtés sont de longueurs $2kuv$, $k(v^2-u^2)$ et $k(v^2+u^2)$ pour trois entiers non nuls $u$, $v$ et $k$ avec $v>u$.
  \end{enumerate}
\end{exo}

% ==================================
\section{Coniques}
% ==================================


%-----------------------------------
\begin{exo} (Ellipse)

  Soit $\mathcal{R}=(O,\vv{u},\vv{v})$ un repère orthogonal. On considère la courbe paramétrée $\gamma:\theta\mapsto (\cos\theta,\sin\theta)_{\mathcal{R}}$.

  \begin{enumerate}
    \item Sous quelle condition cette courbe est un cercle ?
    \item Montrer qu'il s'agit d'une paramétrisation régulière.
    \item Sous quelle condition cette paramétrisation est « par longueur d'arc » ?
    \item Tracer cette courbe.
    \item Déterminer un intervalle maximal d'injectivité $I=[\theta_{\min},\theta_{\max}[$.
  \end{enumerate}
  Soit $\Gamma$ la courbe géométrique de $\gamma|_{I}$. On note $r_1 = \norm{\vv{u}}$ et $r_2 = \norm{\vv{v}}$. Le support $\gamma(I)$ de $\gamma$ est appelé \emph{ellipse} de rayons $r_{1}$ et $r_{2}$, et on va la noter $E$. Le périmètre de $E$ est la longueur $\abs{\Gamma}$.
  \begin{enumerate}[resume]
    \item Montrer les inégalités $2\pi\frac{r_1+r_2}{2}\leq \abs{\Gamma} \leq 2\pi\sqrt{\frac{r_1^2+r_2^2}{2}}$.\\
    \begin{indication}
      Utiliser que la fonction racine est concave.
    \end{indication}
  \end{enumerate}
  Pour la suite on suppose que $r_{1}\neq r_{2}$.
  \begin{enumerate}[resume]
    \item montrer qu'il existe un unique couple (non ordonné) de points $\{M_{1},M_{2}\}$ tel que
    \[
      M_{1}M_{2}=\max_{A,B \in E} AB.
    \]
    \item En déduire qu'il n'y a que deux symétries orthogonales \emph{(lesquelles ?)} qui préservent cette ellipse.
  \end{enumerate}

\end{exo}

%-------------------------------------
\begin{exo} (Théorème de La Hire)

  \sidebyside{.7\linewidth}{
    Soient un disque de rayon $R$ qui roule à l'intérieur d'un cercle de rayon $2R$ et $M$ un point fixe du premier disque. Montrer que $M$ décrit une ellipse et réciproquement que toute ellipse peut être obtenue de cette façon.
  }{
    \vspace{-7mm}\includegraphics[width=.7\linewidth]{hire.pdf}
  }
\end{exo}

%---------------------------------------
\begin{exo} (Miroir parabolique)

  \sidebyside{.7\linewidth}{
    Soient une droite $\mathcal{D}$ et un point $F$ en dehors de cette droite. On rappelle\footnotemark{} qu'une parabole de directrice $\mathcal{D}$ et de foyer $F$ est l'ensemble des points $M$ tels qu'on ait l'égalité des distances
    $$
      d(M,F)= d(M,\mathcal{D}).
    $$
    On se place dans le repère affine orthonormé $(O,\vv{u},\vv{v})$, où $O$ est le point d'intersection de l'axe focal avec la parabole, $\vv{u}$ et $\vv{v}$ sont des vecteurs directeurs respectivement de l'axe focal et de la directrice. Soient $(a,0)$ les coordonnées de $F$ dans ce repère.
  }{\vspace{0mm}
    \includegraphics[width=\linewidth]{parabole_tangente.pdf}
  }
  \footnotetext{Ceci n'est probablement pas un rappel, mais on va se convaincre dans la première question qu'il s'agit bien d'une parabole.}
  \vspace{\topskip}
  \begin{enumerate}
    \item Déterminer l'équation de la parabole dans ce repère.
    \item Donner une courbe paramétrée $f:\mathbb{R}\to\mathbb{R}^{2}$ dont l'image est cette parabole.
    \item Soient $M$ un point de la parabole et $P$ sa projection sur la directrice.\\
    Montrer que la tangente en $M$ est orthogonale à $FP$.
  \end{enumerate}\vspace{-\baselineskip}
  \sidebyside{.7\linewidth}{
    \begin{enumerate}[resume]
      \item En déduire que la tangente en $M$ est la bissectrice de l'angle $\widehat{FMP}$.
      \item Montrer qu'étant donné un miroir concave parabolique, les rayons\\
      parallèles à l'axe focal passent par le foyer.
    \end{enumerate}
  }{\vspace{-11mm}
    \includegraphics[valign=t]{parabole_miroir.pdf}
  }



\end{exo}


\end{document}
