\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m44tds.tex}

\lilleset{titre=TD7 : Transformations et isométries du plan}

\begin{document}


% ==================================
\section{Isométries du plan}
% ==================================

 %-----------------------------------
 % Oral2_CAPES_2015.12
 \begin{exo}[.7]

  Soient $B$ est un point du segment $[AE]$, $ABCD$ et $BEFG$ deux carrés du même côté de $(AE)$. Montrer que les droites $(AG)$ et $(EC)$ sont perpendiculaires (en utilisant une transformation appropriée).
 \end{exo}

%-----------------------------------
\begin{exo}[.7] (théorème de Pappus)

  \emph{(d'après Pappus d'Alexandrie, IV\textsuperscript{e} siècle après J.-C.)}

  Soient $\mathcal{D}$ et $\mathcal{D}'$ deux droites distinctes et des points $A,B,C\in \mathcal{D}$ et $A',B',C'\in \mathcal{D}'$ tels que $(AB')\parallel(A'B)$ et $(BC')\parallel(B'C)$, alors $(AC')\parallel(A'C)$.
\end{exo}

%-----------------------------------
\begin{exo} (Polygone des milieux)

  Dans cet exercice on se place dans $\mathbb{R}^{2}$:

  \begin{enumerate}
    \item Soit $A'B'C'$ un triangle. Montrer qu'il existe un triangle $ABC$, et un seul, tel que $A'$ soit le milieu de $BC$, $B'$ le milieu de $CA$ et $C'$ le milieu de $AB$. Indiquer une construction géométrique de ce triangle.

    \item Soit $A'B'C'D'$ un quadrilatère. Donner une condition nécessaire et suffisante pour qu'il existe un quadrilatère $ABCD$ tel que $A'$ soit le milieu de $AB$, $B'$ le milieu de $BC$, $C'$ le milieu de $CD$ et $D'$ le milieu de $DA$. Ce quadrilatère, s'il existe, est-il unique?

    \item\hard Étant donnés $n$ points $B_{1},\ldots,B_{n}$, peut-on toujours trouver $n$ points $A_{1},\ldots,A_{n}$ tels que $B_{i}$ soit, pour tout $i=1,\ldots,n$, le milieu de $A_{i}A_{i+1}$ (avec la convention $A_{n+1}=A_{1}$ ) ? Donner une construction géométrique des points $A_{i}$ à partir des points $B_{i}$ lorsque la solution existe.\newline
    \begin{indication}
      Comme le montrent les deux premières questions, la solution dépend de la parité de $n$. On pourra considérer la composée des symétries centrales de centres $B_{1},\ldots,B_{n}$.
    \end{indication}

    \item\hard Reprendre la question précédente en traduisant le problème en un système de $n$ équations linéaires à $n$ inconnues dans $\mathbb{C}$. Discuter le rang de ce système selon la parité de $n$, puis le résoudre.

  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Étude d'isométries du plan)

  On considère le plan $\mathbb{R}^{2}$ muni de sa structure euclidienne standard. Soient les applications suivantes:
  \begin{align*}
  f_{1}(x,y) &= (x+3y+1,-3x+y) & f_{2}(x,y) &= \frac{1}{\sqrt{5}}(x-2y+1,2x+y-1)\\
  f_{3}(x,y) &= \frac{1}{5}(3x+4y-1,4x-3y+2) & f_{4}(x,y) &= (x+3,y)\\
  f_{5}(x,y) &= \frac{1}{\sqrt{2}}(x-y+\sqrt{2},-x-y-2+\sqrt{2}) & f_{6}(x,y) &= (\sqrt{x^{2}+y^{2}},x+y)
  \end{align*}
  \begin{enumerate}
    \item Lesquelles de ces applications sont des isométries ?
    \item Lesquelles des isométries sont directes ?
    \item Pour chacune des isométries déterminer sa nature et ses paramètres ?
    \item Décomposer ces isométries en produit de réflexions ?
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}[.49] (Rotation du carré unité)

  Donner un système d'inéquations linéaires dont l'ensemble des solutions est l'image par rotation à $+30°$ autour $(0,0)$ du carré unité.
\end{exo}

% ==================================
\section{Classification des isométries planes}
% ==================================

%-----------------------------------
\begin{exo} (Propriétés des isométries)

\begin{enumerate}
  \item Montrer qu'une isométrie est injective.
  \item Montrer qu'une isométrie $f$ conserve l'alignement et l'ordre sur les droites (plus précisément, les points $A$, $B$ et $C$ sont alignés dans cet ordre si et seulement si leurs images $f(A)$, $f(B)$ et $f(C)$ le sont).
  \item Montrer qu'une isométrie $f$ préserve les barycentres, c.-à-d. $f\big((1- \lambda )A + \lambda B\big) = (1- \lambda )f(A) + \lambda f(B)$ pour tout deux points $A$ et $B$, et $\lambda \in \mathbb{R}$.
  \item Montrer qu'une isométrie est surjective. En déduire que les isométries forment un groupe.
  \item Montrer qu'une isométrie conserve le parallélisme et l'orthogonalité des droites.
\end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Générateurs du groupe des isométries)

  On montre ici que le groupe des isométries du plan est engendré par les réflexions. Plus précisément, toute isométrie s'écrit comme produit d'au plus trois réflexions.
  \begin{enumerate}
    \item Montrer qu'une isométrie fixant deux points distincts $A$ et $B$ fixe tous les points de la droite $(AB)$.
    \item Montrer qu'une isométrie fixant trois points non alignés est égale à l'identité.
    \item Montrer qu'une isométrie fixant deux points distincts est l'identité ou une réflexion dont on précisera l'axe.
    \item Montrer qu'une isométrie fixant un point s'écrit comme produit d'au plus deux réflexions.
    \item Conclure que toute isométrie est produit d'au plus trois réflexions.
    \item Montrer qu'une réflexion ne peut pas s'écrire comme produit de deux réflexions.
    \item Soient $\sigma_1$, $\sigma_2$ et $\sigma_3$ trois réflexions d'axes $\ens{D}_1$, $\ens{D}_2$ et $\ens{D}_3$. Montrer que, si les droites $\ens{D}_i$ sont concourantes ou parallèles, alors le produit $\sigma_1 \sigma_2 \sigma_3$ est une réflexion.
    \item Montrer qu'un produit d'un nombre pair de réflexions peut s'écrire comme produit de deux réflexions.
    \item Montrer que la parité du nombre de réflexions dans une décomposition en produit de réflexions d'une isométrie donnée ne dépend pas de la décomposition considérée.
  \end{enumerate}
  \emph{Cette remarque permet de définir déplacements et antidéplacements sans avoir recours au déterminant de la partie linéaire des isométries.}
\end{exo}

%-----------------------------------
\begin{exo} (Classification des isométries planes)

  On détermine maintenant la nature des isométries du plan, en utilisant que toute isométrie est produit d'au plus trois réflexions.
  \begin{enumerate}
    \item Déterminer les points fixes d'un produit de deux réflexions. Montrer qu'un tel produit est ou bien une translation, ou bien une rotation. Préciser les caractéristiques de ces transformations en fonction des axes des réflexions.
    \item Montrer qu'un produit de trois réflexions est ou bien une réflexion, ou bien une \emph{symétrie glissée}, c.-à-d. le produit d'une réflexion d'axe $\ens{D}$ et d'une translation non triviale dirigée suivant $\ens{D}$.
    \item Dresser la liste des types d'isométries planes, en précisant les points fixes de chacune d'elles.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}[.7] (Isométries du triangle)
  \begin{enumerate}
    \item Déterminer le groupe des isométries d'un triangle dans le plan\footnote{Il s'agit des isométries qui préservent l'ensemble des sommets du triangle.}, la discussion sera menée en fonction des propriétés métriques du triangle.
    \item Même question avec un quadrilatère.
  \end{enumerate}
\end{exo}

\end{document}
