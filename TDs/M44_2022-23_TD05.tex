\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m44tds.tex}

\lilleset{titre=TD5 : Construction à la règle et compas}

\begin{document}


% ==================================
\section{Constructions à la règle et au compas}
% ==================================


\begin{convention}\small
  Un point $M$ du plan est constructible en un pas (sous-entendu à la règle et au compas) à partir d'un ensemble de points $S=\{A_1,\ldots,A_k\}$ si c'est un point d'intersection
  \begin{itemize}
    \item de deux droites distinctes passant chacune par deux points de $S$ : $M \in (A_i A_j)\cap (A_k A_l)$,
    \item ou d'une droite passant par deux points distincts de $S$ et d'un cercle centré en un point de $S$ et passant par un autre point de $S$ : $M \in (A_i A_j)\cap \mathcal{C}(A_k,A_l)$,
    \item ou de deux cercles distincts centrés en des points de $S$ et passant par des points de $S$ : $M \in \mathcal{C}(A_i,A_j)\cap \mathcal{C}(A_k,A_l)$.
  \end{itemize}

  Un point $M$ est dit \emph{constructible à partir de $S$} s'il est constructible en un nombre fini de pas, c'est-à-dire s'il existe $M_1,M_2,\ldots,M_{r-1},M_r=M$ tels que $M_{i+1}$ est constructible en un pas à partir de $S \cup \{M_1,\ldots,M_{i}\}$ pour tout $i=1,\ldots,r-1$.


  « Tracer une droite » signifie « construire deux points distincts sur cette droite », et « tracer un cercle » signifie « construire le centre et un point sur le cercle en question ».
\end{convention}

\paragraph{Principe} En général un exercice de construction à la règle et compas contient trois phases : « analyse », « programme de construction », « justification du programme de construction ». Il y a des exercices qui demandent juste le programme de construction, d'autres qui donnent le programme de construction et demandent des justifications, et d'autres qui demandent les deux (la partie « analyse » est en général réservée au brouillon).

\paragraph{Notations}
\begin{itemize}
  \item $\mathcal{C}(A,B)$ le cercle de centre $A$ qui passe par $B$;
  \item $\mathcal{C}^*(A,B) \coloneqq \mathcal{C}(A,B)\setminus\{B\}$;
  \item $\mathcal{C}[AB]$ est le cercle de diamètre $[AB]$, autrement dit $\mathcal{C}[AB] \coloneqq \mathcal{C}(\frac{A+B}{2},B)$;
   \item $\mathcal{C}(A,BC)$ le cercle de centre $A$ et de rayon $BC$;
  \item $\mathcal{C}(A,B,C)$ le cercle qui passe par $A$,$B$ et $C$ non alignés;
\end{itemize}

\paragraph{Logiciels} Pour vous entraîner avec les constructions à la règle et compas on vous conseille les logiciels suivants:
\begin{itemize}
  \item \href{https://www.euclidea.xyz/}{Euclidea} est un jeu disponible sur mobile et dans un navigateur (en anglais et en russe) qui vous permet de vous familiariser avec les constructions à la règle et compas de façon ludique.
  \item \href{https://sciencevsmagic.net/geo/}{sciencevsmagic} est un site web où vous pouvez vous entraîner à des constructions à partir de deux points.
  \item \href{https://www.geogebra.org/}{GeoGebra} est un logiciel de géométrie qui peut vous aider à faire vos dessins de géométrie plane (et beaucoup plus).
\end{itemize}

%-----------------------------------
\begin{exo}\label{ex:classiques} (Constructions « classiques »)

  Les points nommés sont donnés. Décrire les programmes de constructions « classiques »:
  \begin{enumerate}
    \item Le \textbf{symétrique d'un point} $A$ par rapport à un point $O$.
    \item La \textbf{médiatrice} d'un segment $[AB]$ avec $A \neq B$.
    \item Le \textbf{milieu d'un segment} $[AB]$.
    \item Le \textbf{cercle} $\mathcal{C}[AB]$ de diamètre $[AB]$.
    \item Les \textbf{tangentes} au cercle $\mathcal{C}(A,B)$ passant par un point extérieur $C$.
    \item La \textbf{perpendiculaire} à une droite $(AB)$ \textbf{passant par un point} $C$.
    \item La \textbf{parallèle} à une droite $(AB)$ \textbf{passant par un point} $C$.
    \item Le \textbf{cercle} $\mathcal{C}(A,BC)$ de \textbf{centre} $A$ et de \textbf{rayon} $BC$.
    \item Le \textbf{cercle} $\mathcal{C}(A,B,C)$ \textbf{passant par trois points} non alignés $A$, $B$ et $C$ donnés.
    \item La \textbf{bissectrice} d'un angle $\widehat{BAC}$.
    \item Le \textbf{partage} d'un segment donné $[AB]$ \textbf{en $n$ segments} de même longueur.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}[.49] (proche DS2, 2022)

  Soit $A$ et $B$ deux points donnés et $\mathcal{C}$ le cercle de diamètre $[AB]$. Construire $P \in \mathcal{C}$ et $Q \in [AB)$ tel que $(PQ)$ tangent à $\mathcal{C}$ et $2PQ = AB$.
\end{exo}

%-----------------------------------
\begin{exo}[.49]

  Soit $A$, $B$, $C$ trois points non alignés donnés. Construire $D$ et $E$ sur la droite $(BC)$ tels que le triangle $\tri ADE$ soit équilatéral.
\end{exo}

%-----------------------------------
\begin{exo}[.49]

  Soit $A$, $B$, $C$, $D$ quatre points donnés. Construire, quand c'est possible, un cercle de rayon $AB$ tangent au cercle $\mathcal{C}(A,B)$ et à la droite $(CD)$.
\end{exo}

%-----------------------------------
\begin{exo}[.49]

  Soit $A$, $B$, $C$, $D$ les quatre sommets d'un rectangle donné. Construire les quatre sommets du carré $AB'C'D'$ de même aire que $ABCD$ et tels que $B'\in [AB)$ et $D'\in [AD)$.
\end{exo}

%-----------------------------------
\begin{exo}

  Construire à la règle et au compas le quadrilatère convexe $ABCD$ dont les diagonales se coupent en $I$ et tel que $BI = AI = CI$, $ID = DC$, et $(ID) \perp (DC)$:
  \begin{enumerate}
    \item à partir des points $C$ et $D$ données ;
    \item à partir des points $B$ et $C$ données ;
    \item à partir des points $A$ et $B$ données .
    \item\hard à partir des points $D$ et $A$ données .
    % Considérer la projection orthogonale K de A sur BD, KD=2AK=2DC
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} \emph{(Rattrapage, 2022)}

  Soit $A$, $B$, $C$, $D$ quatre points donnés distincts. On souhaite tracer, quand il existe, un cercle qui passe par $A$ et $B$ et qui est tangent à $(CD)$.
  \begin{enumerate}
    \item Dans le cas où $(AB)\cap(CD)=\{E\}$ avec $A\in]E,B[$, exécuter et justifier le programme de construction suivant (qui ne détaille pas les constructions « classiques » vues dans l'exercice~\ref{ex:classiques}):
    \begin{enumerate}
      \item $E\coloneqq (AB)\cap(CD)$ (avec $A\in]EB[$);
      \item $\mathcal{D}$ la droite perpendiculaire à $(AB)$ qui passe par $A$;
      \item $\{F,F'\} \coloneqq \mathcal{D}\cap\mathcal{C}[EB]$;
      \item $\{T_1,T_2\} \coloneqq \mathcal{C}(E,F)\cap(CD)$;
      \item Les deux cercles qui passent par $A$, $B$ et $T_{i}$, $i=1,2$, conviennent.
    \end{enumerate}
    \item Donner les programmes des constructions dans les autres cas où un tel cercle existe.
    \item Tracer un cercle qui passe par $A$ et qui est tangent à $(CB)$ et $(CD)$.
  \end{enumerate}

\end{exo}


%-----------------------------------
\begin{exo}[.49]

  Soit $ABCD$ un quadrilatère convexe donné. Construire deux points, $M \in [AB)$ et $N \in [AD)$, tels que $C \in [MN]$ et $MC:CN = AB:AD$.\\
  \begin{indication}
    Considérer le point $E$ tel que $AC:CE = AB:AD$.
  \end{indication}
\end{exo}


%-----------------------------------
\begin{exo} (Comment dépasser les bords de la feuille)

  Soient $\Delta$ et $\Delta'$ deux droites qui se coupent en un point $O$ situé en dehors de la feuille.
  \begin{enumerate}
    \item Soit $A$ un point situé sur la feuille. Tracer la droite $(OA)$.
    \item Tracer la bissectrice de l'angle formé par les deux droites (plus précisément de l'angle saillant formé par les demi-droites de la feuille).
  \end{enumerate}

\end{exo}

%-----------------------------------
\begin{exo} (Polygones réguliers)

  \begin{enumerate}
    \item Construire à la règle et au compas un triangle équilatéral, un carré, un hexagone régulier, un octogone régulier : à partir de deux sommets voisins donnés, ou à partir du centre du cercle circonscrit et d'un sommet donnés.
    \item Construire un pentagone régulier.\\
    \begin{indication}
      On peut calculer la valeur de $\cos \frac{2\pi}{5}$ en commençant par remarquer que $0=1+e^{\frac{2i\pi}{5}}+e^{\frac{4i\pi}{5}}+e^{\frac{6i\pi}{5}}+e^{\frac{8i\pi}{5}}=1+2\cos\frac{2\pi}{5}+2\cos\frac{4\pi}{5}$.
    \end{indication}
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (DS1 de M67, mars 2019)

  Soient les deux points $O(0,0)$ et $I(1,0)$ du plan euclidien $\mathbb{R}^2$. Illustrer par un dessin et donner un programme de construction à la règle et au compas à partir de $O$ et $I$ :
  \begin{enumerate}
    \item des points $J(1,1)$ et $K(0,1)$ du carré $OIJK$;
    \item du point $L(1+\sqrt{2},-\frac{2}{3})$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (DS1 de M67, mars 2018)

  Soient $O(0,0)$ et $I(1,0)$ deux points donnés du plan euclidien. Illustrer par un dessin et donner un programme de construction à la règle et au compas à partir de $O$ et $I$:
  \begin{enumerate}
    \item du point $J(0,1)$,
    \item du point $K(\frac23,0)$,
    \item de points $L \in [O,I)$ et $M$ tels que $\tri OLM$ soit rectangle en $M$, $OL=\frac43$ et $LM=\frac23$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Nombres constructibles)

  \begin{convention}\small
    Un point \emph{constructible} est un point constructible à partir de $O=(0,0)$ et $I=(1,0)$.\\
    Un nombre réel $x$ est un \emph{nombre constructible} si le point $(x,0)$ est constructible.
  \end{convention}
  \begin{enumerate}
    \item Montrer qu'un point $(x,y)$ est constructible si et seulement si ses deux coordonnées $x$ et $y$ le sont.
    \item Montrer que tout point de $\Z^2$ est constructible.
    \item Montrer que si $A$ et $B$ sont constructibles, alors la distance $AB$ est constructible.
    \item Montrer que $\sqrt{2}$ et $\sqrt{3}$ sont constructibles. Puis montrer que $\sqrt{n}$ pour $n \in \N$ est constructible.
    \item Montrer que tout rationnel est constructible.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Structure de l'ensemble des nombres constructibles)

  Montrer que l'ensemble des nombres réels constructibles n'est pas $\mathbb{R}$ et qu'il est stable par :
  \begin{itemize}
    \item somme et différence ;
    \item produit et inverse ;
    \item racine carré.
  \end{itemize}
  \emph{Un sous-ensemble de $\mathbb{R}$ qui vérifie les deux premières propriétés est dit \emph{sous-corps}.}
\end{exo}


\end{document}
