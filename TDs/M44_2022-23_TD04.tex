\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m44tds.tex}

\lilleset{titre=TD4 : Triangles et quadrilatères}

\begin{document}


% ==================================
\section{Triangles semblables, Thalès}
% ==================================


%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, page 7
\begin{exo}[.35] \emph{(DS1, 2022)}

  Soient un triangle $\tri ABC$ isocèle en $C$ et un point $P \in (AB)$. Déterminer la longueur de la hauteur $AH$ en fonction des distances $PK$ et $PL$ de $P$ à $(CA)$ et $(CB)$ respectivement.\\
  \begin{indication}
    Discuter selon la position de $P$ par rapport aux points $A$ et $B$.
  \end{indication}
\end{exo}


%-----------------------------------
\begin{exo} (Bissectrice et longueurs)

  Soit $L$ le pied de la bissectrice issue de $A$ dans un triangle $\tri ABC$ (c.-à-d. le point d'intersection de cette bissectrice et du côté opposé $[BC]$).
  \begin{enumerate}
    \item\label{q:bisint} Montrer l'égalité des rapports $AB:AC=LB:LC$.
    \item Montrer que la bissectrice $AL$ coupe le cercle circonscrit en un point $P$ qui est sur la médiatrice de $[BC]$.
    \item Montrer que $AL^{2} = AB \times AC - LB \times LC$
    \item Montrer que la question \ref{q:bisint}) est vraie aussi pour $L$ le pied de la bissectrice extérieure issue de $A$.
  \end{enumerate}
\end{exo}

%-----------------------------------
% Oral2_CAPES_2016.13
\begin{exo}[.49]

  \sidebyside{119mm}{
    Le côté du plus petit carré mesure $16$\,cm et celui du plus grand $36$\,cm. Combien mesure le côté du carré central ?
  }{
    \vspace{-7mm}\includegraphics[width=35mm]{trois_carres_dans_triangle}
  }
\end{exo}


%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, page 7
\begin{exo}[.35]

  Soient un triangle $\tri ABC$, et des points $D,F,H \in [AC]$ et $E,G \in [BC]$ tels que $AB \parallel DE \parallel FG$ et $BD \parallel EF \parallel GH$.
  \begin{enumerate}
    \item Déterminer la longueur $AD$ en sachant que $CF = 2$ et $FD=1$.
    \item Déterminer la longueur $AC$ en fonction de $CH=m$ et $HF=n$.
  \end{enumerate}
\end{exo}



%-----------------------------------
\begin{exo}[.49]

  Dans un triangle $\tri ABC$, $Z$ est un point quelconque de la base $[AB]$. Soit $X \in (BC)$ (resp. $Y \in (AC)$) un point tel que $(AX)\parallel (CZ)$ (resp.$(BY)\parallel (CZ)$). Démontrer que
  \[
    \frac{1}{AX}+\frac{1}{BY} = \frac{1}{CZ}
  \]
\end{exo}


%-----------------------------------
\begin{exo}[.49]

  Soient deux cercles $\mathcal{C}$ et $\mathcal{C'}$ tangent intérieurement en $A$. Une droite passant par $A$ coupe les deux cercles en $M$ et $M'$ respectivement. Montrer que le rapport $AM:AM'$ ne dépend pas du choix de la droite.
\end{exo}


%-----------------------------------
% Oral2_CAPES_2014.14
\begin{exo} (théorème de Desargues, cas concret)

  Dans un repère orthonormé, placer les points $A(2,6)$, $B(2,0)$, $C(-2,2)$, $D(7,1)$, $E(2,2)$, $F(0,4)$ et $G(5,3)$.
  \begin{enumerate}
    \item Démontrer que les points $A$, $C$, $F$ sont alignés, ainsi que les points $A$, $E$, $B$ et $A$, $G$, $D$.

    \item
    \begin{enumerate}
      \item Déterminer les coordonnées du point $I$, intersection des droites $(EF)$ et $(BC)$.
      \item On appelle $J$ le point d’intersection de $(EG)$ et $(BD)$, et $H$ le point d'intersection de $(FG)$ et $(CD)$. On admet que $J(-13,-3)$ et $H(25,-1)$. Démontrer que les points $I$, $J$, $H$ sont alignés.
    \end{enumerate}
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (théorème de Desargues)

  \emph{(d'après Girard Desargues, alias S.G.D.L., XVII\textsuperscript{e} siècle à Lyon)}

  Soient $\tri ABC$ et $A'B'C'$ deux triangles non dégénérés, sans sommets communs, et tels que $(A'B') \parallel (AB)$, $(A'C') \parallel (AC)$ et $(B'C') \parallel (BC)$.
  \begin{enumerate}
    \item Montrer que les triangles $\tri ABC$ et $A'B'C'$ sont semblables.
    \item Montrer que de plus les droites $(AA')$, $(BB')$ et $(CC')$ sont parallèles ou concourantes.
  \end{enumerate}
\end{exo}

% ==================================
\section{Dans un triangle}
% ==================================

%-----------------------------------
\begin{exo} (Triangle rectangle)

  Soit $\tri ABC$ un triangle rectangle en $A$, et soit $H$ le pied de la hauteur issue de $A$ (autrement dit, le projeté orthogonal de $A$ sur $(BC)$). Montrer que
  \[
    BA^2=BH \cdot BC,\ \ CA^2=CH \cdot CB,\ \ AH^2=BH \cdot CH.
  \]
  \begin{indication}
    On pourra utiliser les triangles semblables, ou la trigonométrie vue au collège.
  \end{indication}\\
  \begin{remarque}
    Ces équations sont très importantes pour les constructions à la règle et au compas.
  \end{remarque}
\end{exo}

%-----------------------------------
\begin{exo} (Triangle isocèle)

  Soit $\tri ABC$ un triangle isocèle avec $AB=AC > BC$. On porte des points $D$ sur $(AB)$, avec $B$ entre $A$ et $D$, et $E$ sur $(BC)$, avec $C$ entre $B$ et $E$, et tels que $BD=CE=AB-BC$.

  Montrer que $\tri ADE$ est un triangle isocèle.
\end{exo}

%-----------------------------------
\begin{exo} (Points remarquables dans le triangle)

  Soit $\tri ABC$ un triangle non dégénéré.
  \begin{enumerate}
    \item Montrer que les trois bissectrices sont concourantes. Montrer que leur point commun est centre d'un \emph{cercle inscrit} dans le triangle $\tri ABC$, et qu'il n'y a qu'un seul tel centre (et un seul tel cercle).
    \item Montrer que la bissectrice d'un angle et les bissectrices extérieures des deux autres angles sont également concourantes. \emph{Ce point est le centre d'un des trois cercles exinscrits.}
    \item Montrer que les trois médiatrices du triangle sont concourantes. Montrer que leur point commun est \emph{centre du cercle circonscrit} au triangle $\tri ABC$.
    \item Montrer que les trois médianes sont concourantes en un point situé au tiers de chacune d'elles en partant de la base correspondante. On appelle \emph{centre de gravité} ou \emph{barycentre}\footnote{ou \emph{isobarycentre}, ou \emph{centroïde}.} leur point d'intersection.
    \item Montrer que les trois hauteurs sont concourantes. On appelle \emph{orthocentre} leur point d'intersection.
    \item Montrer que le centre du cercle circonscrit, l'orthocentre et le centre de gravité sont alignés (on appelle \emph{droite d'Euler} la droite passant par ces trois points).
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Coordonnés barycentriques de points remarquables)

  Déterminer les barycentres des points remarquables du triangle : barycentre, orthocentre, centre du cercle circonscrit, centre du cercle inscrit, les centre des cercles exinscrits.
\end{exo}

%-----------------------------------
\begin{exo} (Rayons des cercles circonscrit et inscrit)

  Soit $\tri ABC$ un triangle de longueurs de côtés $a$, $b$ et $c$ et de mesures des angles respectifs $\alpha$, $\beta$ et $\gamma$.
  \begin{enumerate}
    \item Exprimer le rayon $R$ du cercle circonscrit en fonction des longueurs des côtés et des angles du triangle.
    \item Exprimer le rayon $r$ du cercle inscrit en fonction du périmètre et de l'aire du triangle.
    \item Exprimer le rayon $r$ du cercle inscrit en fonction des longueurs des côtés et des angles du triangle.
    \begin{indication}
      Utiliser que $\mathcal{A}_{\tri ABC} = \frac{1}{2}ab\sin \gamma$.
    \end{indication}
    \item Exprimer le rapport $\frac{R}{r}$ en fonction des angles du triangle, et le produit $Rr$ en fonction des longueurs des côtés.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}[.7] (Triangle orthique)

  Soit $\tri ABC$ un triangle non rectangle. On note respectivement $H_A$, $H_B$ et $H_C$ les pieds des hauteurs issues de $A$, $B$ et $C$. Montrer que les bissectrices du triangle $\tri H_AH_BH_C$ sont les hauteurs du triangle $\tri ABC$.
\end{exo}

%-----------------------------------
\begin{exo} (Tout triangle est isocèle)

  On donne ici un argument pour établir que tout triangle est isocèle (dû à W.W. Rouse Ball).
  Soit $\tri ABC$ un triangle quelconque. Soit $D$ le point d'intersection de la bissectrice de l'angle $\widehat{BAC}$ avec la médiatrice du côté opposé $[BC]$. Soient $E,F$ et $G$ les projetés orthogonaux de $D$ sur $[BC]$,$[AB]$ et $[AC]$.
  \sidebyside{11cm}{
    \begin{enumerate}
      \item Montrer que $DF=DG$ et $AF=AG$.
      \item Montrer que $DB=DC$, puis que $FB=GC$.
      \item En déduire que $\tri ABC$ est isocèle, puis qu'il est équilatéral.
      \item Comment expliquer cela ?
    \end{enumerate}
  }{
    \vspace{-11mm}\includegraphics[width=35mm]{img_isocele_ball}
  }
\end{exo}


% ==================================
\section{Quadrilatères}
% ==================================

%-----------------------------------
% Posamentier & Salkind - Challenging Problems in Geometry, page 7
\begin{exo}

  La longueur de la grande base d'un trapèze est $97$. La longueur du segment joignant les milieux des diagonales est $3$.
  \begin{enumerate}
    \item Trouvez la longueur de la petite base.
    \item Trouvez la longueur du segment joignant les milieux des deux côtés latérales.
  \end{enumerate}
\end{exo}

%-----------------------------------
% Oral2_CAPES_2018.01
\begin{exo}

  Soit $ABCD$ un trapèze avec bases $[AB]$ et $[CD]$ et $O$ l'intersection des diagonales. On note $M$ et $N$ les intersection de la droite parallèle aux bases passant par $O$ avec les côtés $[AD]$ et $[BC]$.
  \begin{enumerate}
    \item Montrer que $O$ est le milieu de $[MN]$.
    \item Parmi les trapèzes avec longueurs des bases $AB=a$ et $CD=b$ fixées, lesquels sont ceux où la distance de $O$ à $(AD)$ est maximale ?
    \item\hard Que peut-on dire sur la somme des deux distances $d(O,(AD))+d(O,(BC))$ sous la même contraintes des longueurs des bases ?
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Carré dans un triangle)

  Soit $\tri ABC$ un triangle \emph{acutangle}\footnote{À angles aigus.}.
  \begin{enumerate}
    \item Montrer qu'il existe un carré $IJKL$ avec $I,J \in [AB]$, $K \in [BC]$ et $L \in [CA]$. En donner une construction.
    \item Un tel carré est-il unique ?
    \item Que se passe-t-il si le triangle a un angle obtus ?
  \end{enumerate}
\end{exo}


%-----------------------------------
\begin{exo} (Parallélogrammes)

  On dit que quadrilatère $ABCD$ est \emph{convexe} s'il est \emph{non dégénéré} (sans trois sommets alignés), \emph{non croisé} (les intérieurs des côtés (opposés) ne se rencontrent pas) et sans sommet à l'intérieur du triangle formé par les trois autres\footnote{Si $D$ est à l'intérieur de $\tri ABC$ on peut utiliser la terminologie non standard que « $ABCD$ est \emph{concave} en $D$ ».}.
  \begin{enumerate}
    \item Montrer qu'un quadrilatère $ABCD$ est convexe si et seulement si les \emph{diagonales} $[AC]$ et $[BD]$ se rencontrent.
    \begin{indication}
      Discuter le type de quadrilatère en fonction de la position de $D$ par rapport aux droites des côtes de $\tri ABC$.
    \end{indication}
    \item Montrer que pour un quadrilatère convexe $ABCD$ les conditions suivantes sont équivalentes :
    \begin{enumerate}
      \item c'est un parallélogramme ;
      \item les angles opposés sont égaux ;
      \item les côtés opposés sont égaux ;
      \item deux côtés opposés sont parallèles et égaux ;
      \item ses diagonales $[AC]$ et $[BD]$ se coupent en leur milieu.
    \end{enumerate}
    \item Montrer qu'un parallélogramme est un rectangle (resp. un losange) si et seulement si ses diagonales sont égales (resp. perpendiculaires).
    \item Montrer que les bissectrices d'un parallélogramme forment un rectangle. À quelle condition forment-elles un carré ?
    \item Montrer que les milieux des côtés d'un quadrilatère (convexe) sont les sommets d'un parallélogramme.
  \end{enumerate}
\end{exo}

% ==================================
\section{Quadrilatères inscrits}
% ==================================

\bigskip
\textbf{Résultat du cours:} Quatre points distincts $A,B,C,D$ sont cocycliques si et seulement si on a l'égalité modulo $\pi$ entre angles \textbf{orientés}: $\ang ABC = \ang ADC \modulo{\pi}$.
\bigskip

%-----------------------------------
\begin{exo} (Quadrilatère inscrit)

    Soit $ABCD$ un quadrilatère non croisé.
    \begin{enumerate}
      \item Montrer que si $ABCD$ est inscrit dans un cercle, alors $ABCD$ est convexe (et ses diagonales se coupent en un point).
      \item Montrer l'équivalence des assertions suivantes :
      \begin{enumerate}
        \item $ABCD$ est inscrit dans un cercle.
        \item $ABCD$ est convexe et $\widehat{ABC}+\widehat{CDA} = \pi$ (ou $\widehat{DAB}+\widehat{BCD} = \pi$).
        \item $[AC]$ et $[BD]$ se coupent en $S$ et $AS \times SC = BS \times SD$.
        % \item $AB\times CD + BC\times DA = AC\times BD$ (c'est le \emph{théorème de Ptolémée}).
      \end{enumerate}
    \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}[.49]

  Soient $\tri ABC$ un triangle et $H$ son orthocentre. Soit $H'$ le symétrique de $H$ par rapport à $(BC)$ Montrer que $H'$ est sur le cercle circonscrit à $\tri ABC$.
\end{exo}

%-----------------------------------
\begin{exo}[.7]

  Soient $\tri ABC$ un triangle et $P$, $Q$, $R$ trois points situés respectivement sur $[BC]$, $[CA]$ et $[AB]$. Montrer que les cercles circonscrits aux triangles $AQR$, $BRP$ et $CPQ$ ont un point commun.\\
  \begin{indication}
    Attention au cas où deux des cercles se touchent.
  \end{indication}
\end{exo}

%-----------------------------------
\begin{exo} (Cercle d'Euler)

  Soit le triangle $\tri A_{1}A_{2}A_{3}$. Montrer que les 9 points remarquables du triangle $\tri ABC$ suivants sont cocycliques :
  \begin{itemize}
    \item Les trois milieux $I_{i}$ des trois côtés du triangle ;
    \item Les pieds $H_i$ des trois hauteurs ;
    \item Les milieux $J_{i}$ de chacun des trois segments reliant l'orthocentre $H$ à un sommet du triangle $A_{i}$.
  \end{itemize}
  \emph{Le cercle passant par ces points est appelé « cercle d'Euler ».}
\end{exo}

\end{document}
