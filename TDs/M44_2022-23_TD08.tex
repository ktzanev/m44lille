\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m44tds.tex}

\lilleset{titre=TD8 : Nombres complexes}

\begin{document}


% ==================================
\section{Droites et cercles}
% ==================================

%-----------------------------------
\begin{exo} (Droites)

  On considère le plan complexe $\mathbb{C}$ où tout point est identifié avec son affixe.
  \begin{enumerate}
    \item Étant donnés deux points distincts $A$ et $B$ du plan complexe d'affixes $a$ et $b$, donner une condition sur $a\overline{b}$ pour que la droite $AB$ passe par $O$ d'affixe $0$. Préciser quand $O$ est entre $A$ et $B$, et quand il ne l'est pas.
    \item Montrer que toute droite réelle de $\mathbb{C}$ est définie par une équation complexe de la forme
      \[
        \big\{z \in \mathbb{C} \;\big|\; \overline{\beta}z+\beta\overline{z}+\gamma=0\big\}
      \]
      où $\beta \in \mathbb{C}^{*}$ et $\gamma \in \mathbb{R}$.
    \item Donner une condition sur les coefficients des équations complexes pour que deux droites soient parallèles.
  \end{enumerate}
  Soit $\mathcal{D}$ la droite dont l'équation réelle est $x+2y=1$.
  \begin{enumerate}[resume]
    \item Déterminer une équation complexe de la droite $\mathcal{D}$.
    \item Déterminer toutes les équations complexes qui définissent la même droite $\mathcal{D}$.
    \item Déterminer toutes les équations complexes qui définissent une droite parallèle à $\mathcal{D}$.
    \item Déterminer la droite vectorielle $\vv{\mathcal{D}}$, parallèle à $\mathcal{D}$ qui passe par $0$.
    \item Déterminer les équations complexes qui définissent la droite parallèle à $\mathcal{D}$ qui passe par $A=(2,1)$.
    \item Déterminer une équation de la droite qui passe par les points $C=(2,3)$ et $D=(4,-3)$. Est-elle parallèle à $\mathcal{D}$?
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} \emph{(Rattrapage, 2022)}

  Soient $ABCD$ un quadrilatère convexe et $O$ point intérieur tel que les triangles $\tri AOB$ et $\tri COD$ soient isocèles rectangle en $O$. On note $M$ le milieu de $[AD]$ et $H$ le pied de la hauteur issue de $O$ dans le triangle $BOC$.
  \begin{enumerate}
    \item Monter que les points $M$, $O$ et $H$ sont alignés.
    \item Montrer que $2OM=BC$.
  \end{enumerate}

\end{exo}

%-----------------------------------
\begin{exo} (Équation d'un cercle)

  Montrer que tout cercle du plan complexe est défini par une équation de la forme
    $$
      z\overline{z}-a\overline{z}-\overline{a}z+c=0,
    $$
  où $a$ est un nombre complexe et $c$ un réel vérifiant $c \leq |a|^{2}$. Montrer que réciproquement toute équation de ce type est celle d'un cercle.
\end{exo}

%-----------------------------------
\begin{exo} (Encore des équations)
  \begin{enumerate}
    \item Discuter selon les valeurs de $\alpha,\gamma \in \mathbb{R}$ et $\beta \in \mathbb{C}$ quel est le sous-ensemble de $\mathbb{C}$ défini par l'équation
      $$
        \alpha.z\overline{z}+\beta\overline{z}+\overline{\beta}z+\gamma=0.
      $$
    \item Soit $\lambda$ un nombre réel positif, décrire géométriquement l'ensemble
      $$
        E_{\lambda}=\lbrace z\in\mathbb{C}:\vert z-a \vert =\lambda \vert z-b \vert \rbrace .
      $$
    \item Représenter graphiquement les solutions des trois équations $\vert z-1 \vert=\vert z-i z\vert=\vert z-i \vert$. Déterminer les affixes des points qui sont dans ces trois ensembles.
  \end{enumerate}
\end{exo}


% ==================================
\section{Polygones réguliers}
% ==================================

%-----------------------------------
\begin{exo} (Conditions géométriques)
  \begin{enumerate}
    \item Donner une condition nécessaire et suffisante sur les affixes $a$, $b$, $c$ de trois points $A$, $B$ et $C$ du plan complexe pour que le triangle $ABC$ soit équilatéral.
    \item Donner une condition nécessaire et suffisante sur les affixes $a$, $b$, $c$, $d$ de quatre points $A$, $B$, $C$ et $D$ du plan complexe pour que le quadrilatère $ABCD$ soit un carré.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Triangle de Napoléon et point de Torricelli)

  Soient $\tri ABC'$, $\tri AB'C$ et $\tri A'BC$ trois triangles équilatéraux construit à l'extérieur d'un triangle quelconque $\tri ABC$ avec des barycentres respectifs $G_{C}$, $G_{B}$ et $G_{A}$.
  \begin{enumerate}
     \item Montrer que le triangle $\tri G_{A}G_{B}G_{C}$ est équilatéral \emph{(appelé « triangle de Napoléon »)}.
     \item Montrer $(AA')$, $(BB')$ et $(CC')$ sont concourantes en un point noté $I$ (et appelé\footnote{Appelé également « point de Fermat » ou « point de Steiner » ou « premier point isogonique ».} « le point de Torricelli » du triangle $\tri ABC$).
     \item Montrer que quand tous les angles de $\tri ABC$ sont inférieurs à $120°$ le point de Torricelli est l'unique point d'où les trois côtés du triangle sont vu sous le même angle.
   \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} \emph{(DS2, 2022)}

  \sidebyside{11cm}{
    À l'extérieur d'un triangle $ABC$, on construit trois carrés de bases les côtés et de centres $P$, $Q$ et $R$. Montrer que les segments $AP$ et $QR$ (resp. $BQ$ et $RP$, $CR$ et $PQ$) sont orthogonaux et de même longueur. En déduire que les droites $AP$, $BQ$ et $CR$ sont concourantes.
  }{
    \vspace{-2\baselineskip}\includegraphics[width=5cm]{triangle_et_carres.pdf}
  }
\end{exo}

%-----------------------------------
\begin{exo} \emph{(DS2, 2021)}

  \sidebyside{11cm}{
    On construit à l'extérieur d'un parallélogramme $ABCD$ quatre carrés de bases les côtés et de centres $M$, $N$, $P$ et $Q$. Montrer que $MNPQ$ est un carré.
  }{
    \vspace{-2\baselineskip}\includegraphics[width=5cm]{parallelogram_et_carres.pdf}
  }
\end{exo}

% ==================================
\section{Points cocycliques}
% ==================================

%-----------------------------------
\begin{exo} (Points cocycliques et le théorème de Ptolémée)

  On se donne $4$ points distincts $A$, $B$, $C$ et $D$ du plan complexe d'affixes $a$, $b$, $c$ et $d$ respectivement.
  \begin{enumerate}
    \item Montrer que l'on a toujours
      $$
        |AC|.|BD| \leq |AB|.|CD|+|AD|.|BC|
      $$
      avec égalité si et seulement s'il existe un réel $\lambda > 0$ tel que $(b-a)(d-c)=\lambda (d-a)(c-b)$.
    \item Déduire, de la question précédente, que si $ABCD$ est un quadrilatère convexe, alors le cas d'égalité est équivalent à
      $$
        \arg(\frac{b-a}{d-a}) = \arg(\frac{b-c}{d-c}) \;\modulo{\pi}.
      $$
    \item Montrer le théorème de Ptolémée :

    \textit{Un quadrilatère convexe est inscrit dans un cercle si et seulement si le produit des longueurs de ses diagonales est égal à la somme des produits des longueurs des côtés opposés.}
  \end{enumerate}
 \end{exo}

%-----------------------------------
\begin{exo} (Birapports)

  Soient $4$ points du plan complexe avec affixes $a$, $b$, $c$, $d$ distincts deux à deux. On définit leur birapport comme étant la quantité:
  $$
    [a,b,c,d]=\frac{a-c}{b-c}\cdot\frac{b-d}{a-d} \in \mathbb{C}.
  $$
  \begin{enumerate}
    \item Montrer que si $4$ points sont alignés alors leur birapport est un nombre réel. Dans quel cas ce birapport est positif ?
    \item Montrer que si $4$ points sont cocycliques alors leur birapport est un nombre réel. Dans quel cas ce birapport est positif ?
    \item Montrer que le birapport de $4$ points est réel si et seulement si les $4$ points sont alignés ou cocycliques.
    \item Prouver que si $4$ points d'affixes non nulles sont alignés alors leurs images par $1/\overline{z}$ sont alignées ou cocycliques.
    \item Montrer que les homothéties de rapport non nul, les rotations et les translations préservent les birapports, et que les symétries les transforment en leurs conjugués.
    \item Montrer que les points du plan euclidien $(-2,6)$, $(1,7)$, $(4,6)$ et $(6,2)$ sont cocycliques.
  \end{enumerate}
\end{exo}

% ==================================
\section{Inversion}
% ==================================

%-----------------------------------
\begin{exo} (Inversions)

  Une inversion $i(\Omega,r)$ de centre $\Omega$ et de rapport $r \in \mathbb{R}^{*}_{+}$ est une application du plan $P$ privé de $\Omega$ dans $P$ qui à tout point $M$ associe le point $M'$ tel que
  \begin{itemize}
    \item $M'$ soit sur la droite $\Omega M$,
    \item $\overline{\Omega M}.\overline{\Omega M'}=r^2$.
  \end{itemize}
  \begin{enumerate}
    \item Déterminer l'image de $i(\Omega,r)$.
    \item Montrer que l'inversion est une involution.
    \item En identifiant le plan $P$ avec $\mathbb{C}$, donner l'expression analytique de l'inversion $i(0,1)$, puis, plus généralement, de $i(\omega,r)$ avec $\omega \in \mathbb{C}$ et $r \in \mathbb{R}^{*}_{+}$.
    \item Montrer que chaque point du cercle de centre $\Omega$ et de rayon $r$ est invariant par l'inversion.
    \item Montrer qu'une inversion transforme un birapport en son conjugué.
    \item Montrer que l'inversion préserve les droites\footnote{privé du point $\Omega$ qui n'est pas dans le domaine de définition de $i(\Omega,r)$} qui passent par $\Omega$.
    \item Montrer qu'elle envoie une droite qui ne passe par $\Omega$ sur un cercle\footnotemark[\value{footnote}] qui passe par $\Omega$.
    \item Montrer aussi qu'elle envoie tout cercle qui ne passe pas par $\Omega$ sur un cercle qui ne passe pas par $\Omega$.
    \item Quels sont les cercles invariants par $i(\Omega,r)$ ?
  \end{enumerate}
\end{exo}

%-----------------------------------
% Sortais, Sortais - La géométrie du triangle. Exercices résolus, page 188
\begin{exo}

  Soit $\mathcal{C}$ un cercle de centre $I$ et $\delta$ une droite tangente à $\mathcal{C}$ en un point $Q$. Soient un point $P \notin \mathcal{C}\cup \delta$. Pour tout point $M\neq P$ on note $M'$ le point de la droite $(PM)$ tel que $\overline{PM}\times\overline{PM'}=P_{\mathcal{C}}(P)$. Montrer que quand $M$ parcours la droite $\delta$, $M'$ parcours un cercle, tangent à $\mathcal{C}$, privé du point $P$, qui est sur ce cercle.
\end{exo}

%-----------------------------------
\begin{exo}

  \sidebyside{11cm}{
    Deux cercles $\mathcal{C}$ et $\mathcal{C}'$ sont tangents intérieurement en $O$. On considère une famille de cercles deux à deux tangents entre eux et tangents à $\mathcal{C}$ et $\mathcal{C}'$. Montrer que les points de contact de ces cercles entre eux sont situés sur un même cercle tangent en $O$ aux deux cercles $\mathcal{C}$ et $\mathcal{C}'$.
  }{
    \vspace{-7mm}\includegraphics[width=49mm]{cercles_entre_deux_cercles.pdf}
  }
\end{exo}

%-----------------------------------
\begin{exo} (Homographies)

  Une homographie est une application de $\mathbb{C}$ dans $\mathbb{C}$ de la forme
  $$
    z\mapsto \frac{az+b}{cz+d}
  $$
  avec $a,b,c,d$ complexes, tels que $ad-bc\neq 0$.
  \begin{enumerate}
    \item Montrer que toute homographie est la composée de translations, d'homothéties et au plus d'une inversion et d'une réflexion.
    \item Montrer que toute homographie envoie droites ou cercles sur droites ou cercles.
    \item Montrer que les homographies forment un groupe.
  \end{enumerate}
\end{exo}

\end{document}
